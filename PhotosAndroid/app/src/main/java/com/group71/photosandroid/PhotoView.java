package com.group71.photosandroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.group71.photosandroid.databinding.ActivityPhotoViewBinding;

import java.io.IOException;
import java.util.ArrayList;

public class PhotoView extends AppCompatActivity {
    final Context context = this;

    private AppBarConfiguration appBarConfiguration;
    private ActivityPhotoViewBinding binding;

    ImageView imageView;
    AlbumList listOfAlbums;

    ListView listTags;
    int currentTag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityPhotoViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        // Unpack albumList parcel
        Intent intent = getIntent();
        listOfAlbums = intent.getParcelableExtra("Album List");

        setPhoto();
        refreshList();

        listTags.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                currentTag = i;
                deleteTag();

            }
        });


    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.photo_view_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.deletePhoto:
                //Toast.makeText(this, "Add Photo Selected", Toast.LENGTH_SHORT).show();
                deletePhoto();
                try {
                    DataStorage.writeData(listOfAlbums);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finish();
                return true;
            case R.id.addTag:
                addTag();
                try {
                    DataStorage.writeData(listOfAlbums);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(this, "Add Tag Selected", Toast.LENGTH_SHORT).show();
                refreshList();
                return true;
            case R.id.deleteTag:
                LayoutInflater li = LayoutInflater.from(context);
                View newAlbumView = li.inflate(R.layout.delete_tag_message,null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setView(newAlbumView);

                alertDialogBuilder.setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            case R.id.movePhoto:
                movePhoto();

                return true;

            default:
                return false;
        }
    }

    public void setPhoto() {
        imageView = new ImageView(this);
        imageView = (ImageView) findViewById(R.id.imageView);

        imageView.setImageURI(listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.get(listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto).location);
        imageView.setAdjustViewBounds(true);
        imageView.setMaxHeight(700);
    }

    public void deletePhoto(){
        listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.remove(listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto);
        Bundle bundle = new Bundle();
        bundle.putParcelable("Album List", listOfAlbums);
        System.out.println(bundle == null);
        Intent currentIntent = new Intent();
        currentIntent.putExtras(bundle);
        setResult(RESULT_OK, currentIntent);
        return;
    }

    public void onBackPressed(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("Album List", listOfAlbums);
        System.out.println(bundle == null);
        Intent currentIntent = new Intent();
        currentIntent.putExtras(bundle);
        setResult(RESULT_OK, currentIntent);
        finish();
    }

    public void addTag(){


        LayoutInflater li = LayoutInflater.from(context);
        View newAlbumView = li.inflate(R.layout.create_tag,null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(newAlbumView);

        EditText userInput = (EditText) newAlbumView.findViewById(R.id.edittxtAlbumName);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Person", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if( (userInput.getText().toString() != null) && (userInput.getText().toString().length() != 0))
                        {
                            createTag("Person", userInput.getText().toString());
                            refreshList();
                        }
                        else
                        {
                            CharSequence text = "Invalid Name";
                            int duration = Toast.LENGTH_SHORT;
                            Toast.makeText(context, text, duration).show();
                        }
                    }
                })
                .setNegativeButton("Location", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id){
                        if( (userInput.getText().toString() != null) && (userInput.getText().toString().length() != 0))
                        {
                            createTag("Location", userInput.getText().toString());
                            refreshList();
                        }
                        else
                        {
                            CharSequence text = "Invalid Name";
                            int duration = Toast.LENGTH_SHORT;
                            Toast.makeText(context, text, duration).show();
                        }
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void createTag(String key, String value){
        Boolean containsLocation = false;
        Album currentAlbum = listOfAlbums.albumList.get(listOfAlbums.albumSelected);
        Photo currentPhoto = currentAlbum.photos.get(currentAlbum.selectedPhoto);
        ArrayList<Tag> tags = currentPhoto.tags;

        if(key.equals("Location")){
            for(int i = 0; i < tags.size(); i++){
                if(tags.get(i).category.equals("Location")){
                    containsLocation = true;
                }
            }
            if(containsLocation == false){
                tags.add(new Tag(key, value));
                try {
                    DataStorage.writeData(listOfAlbums);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
                Toast.makeText(context, "Photo already has Location tag.", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            tags.add(new Tag(key, value));
            try {
                DataStorage.writeData(listOfAlbums);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //return;
    }

    public void refreshList(){
        Album currentAlbum = listOfAlbums.albumList.get(listOfAlbums.albumSelected);
        Photo currentPhoto = currentAlbum.photos.get(currentAlbum.selectedPhoto);
        ArrayList<Tag> tags = currentPhoto.tags;
        listTags = new ListView(this);
        listTags = (ListView) findViewById(R.id.listTags);
        //listTags.removeAllViews();

        String[] tagStrings = new String[tags.size()];
        for(int i = 0; i < tags.size(); i++){
            tagStrings[i] = tags.get(i).category + ": " + tags.get(i).value;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tagStrings);
        listTags.setAdapter(adapter);
    }

    public void nextPhoto(View view){
        Album currentAlbum = listOfAlbums.albumList.get(listOfAlbums.albumSelected);
        Photo currentPhoto = currentAlbum.photos.get(currentAlbum.selectedPhoto);
        ArrayList<Tag> tags = currentPhoto.tags;
        if(currentAlbum.photos.size() == 0){}
        else if(currentAlbum.selectedPhoto == currentAlbum.photos.size() - 1){
            listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto = 0;
        }
        else{
            listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto++;
        }
        setPhoto();
        refreshList();
    }

    public void previousPhoto(View view){
        Album currentAlbum = listOfAlbums.albumList.get(listOfAlbums.albumSelected);
        Photo currentPhoto = currentAlbum.photos.get(currentAlbum.selectedPhoto);
        ArrayList<Tag> tags = currentPhoto.tags;
        if(currentAlbum.photos.size() == 0){}
        else if(currentAlbum.selectedPhoto == 0){
            listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto = currentAlbum.photos.size() - 1;
        }
        else{
            listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto--;
        }
        setPhoto();
        refreshList();
    }

    public void deleteTag(){
        LayoutInflater li = LayoutInflater.from(context);
        View newAlbumView = li.inflate(R.layout.delete_tag,null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(newAlbumView);

        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Album currentAlbum = listOfAlbums.albumList.get(listOfAlbums.albumSelected);
                        Photo currentPhoto = currentAlbum.photos.get(currentAlbum.selectedPhoto);
                        ArrayList<Tag> tags = currentPhoto.tags;
                        listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.get(currentAlbum.selectedPhoto).tags.remove(currentTag);
                        refreshList();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id){
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    public void movePhoto(){
        Intent intent = new Intent(PhotoView.this, MovePhoto.class);
        intent.putExtra("Album List", (Parcelable) listOfAlbums);
        startActivityForResult(intent, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if(resultCode != RESULT_OK){
            return;
        }

                Bundle photoBundle = intent.getExtras();
                listOfAlbums = (AlbumList) photoBundle.get("Album List");
                refreshList();
                return;



    }
}