package com.group71.photosandroid;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class Photo implements Parcelable, Serializable {
    private static final long serialVersionUID = 1L;
    Uri location;
    String caption;
    ArrayList<Tag> tags;

    // Parcelable Constructors + Implementation Methods
    protected Photo(Parcel in) {
        location = in.readParcelable(Uri.class.getClassLoader());
        caption = in.readString();
        tags = in.createTypedArrayList(Tag.CREATOR);
    }

    public Photo(Uri newPhoto){
        location = newPhoto;
        caption = newPhoto.getLastPathSegment();
        tags = new ArrayList<Tag>();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(location, i);
        parcel.writeString(caption);
        parcel.writeTypedList(tags);
    }
}
