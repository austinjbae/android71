package com.group71.photosandroid;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

//this is a comment for the push test

public class Album implements Parcelable, Serializable {
    private static final long serialVersionUID = 1L;
    String name;
    ArrayList<Photo> photos;
    int selectedPhoto = 0;

    // custom constructors
    public Album(String name) {
        this.name = name;
        this.photos = new ArrayList<Photo>();
    }

    public Album(String name, ArrayList<Photo> photos) {
        this.name = name;
        this.photos = photos;
    }

    protected Album(Parcel in) {
        name = in.readString();
        photos = in.createTypedArrayList(Photo.CREATOR);
    }

    public void addPhoto(Uri newPhoto){
        photos.add(new Photo(newPhoto));
    }

    // Parcelable Constructors + Implementation Methods
    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeTypedList(photos);
    }

    // Setters and Getters
    public void setName(String newName){
        this.name = newName;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Photo> getPhotos(){
        return photos;
    }
}
