package com.group71.photosandroid;

import android.content.Context;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class DataStorage {

    static final String storeDir = "data";
    static final String storeFile = "albumdata.dat";

    public static void writeData(AlbumList albumList) throws IOException {
        File file = new File("/sdcard/save_object.bin");
        FileOutputStream out = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(out); //context.openFileOutput(storeFile, Context.MODE_PRIVATE));
        oos.writeObject(albumList);
        oos.close();
        out.close();
    }

    public static AlbumList readData() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("sdcard" + File.separator +"save_object.bin")));
        AlbumList returnList = (AlbumList)ois.readObject();
        ois.close();

        return returnList;
    }
}
