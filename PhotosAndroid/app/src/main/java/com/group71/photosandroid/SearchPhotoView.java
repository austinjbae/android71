package com.group71.photosandroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.group71.photosandroid.databinding.ActivityPhotoViewBinding;

import java.util.ArrayList;

public class SearchPhotoView extends AppCompatActivity {
    final Context context = this;

    private AppBarConfiguration appBarConfiguration;
    private ActivityPhotoViewBinding binding;

    ImageView imageView;
    AlbumList listOfAlbums;

    Album currentAlbum;

    ListView listTags;
    int currentTag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityPhotoViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        // Unpack albumList parcel
        Intent intent = getIntent();
        currentAlbum = intent.getParcelableExtra("Current Album");

        setPhoto();
        refreshList();


    }

    public void setPhoto() {
        imageView = new ImageView(this);
        imageView = (ImageView) findViewById(R.id.imageView);

        imageView.setImageURI(currentAlbum.photos.get(currentAlbum.selectedPhoto).location);
        imageView.setAdjustViewBounds(true);
        imageView.setMaxHeight(700);
    }

    public void refreshList(){
        Photo currentPhoto = currentAlbum.photos.get(currentAlbum.selectedPhoto);
        ArrayList<Tag> tags = currentPhoto.tags;
        listTags = new ListView(this);
        listTags = (ListView) findViewById(R.id.listTags);
        //listTags.removeAllViews();

        String[] tagStrings = new String[tags.size()];
        for(int i = 0; i < tags.size(); i++){
            tagStrings[i] = tags.get(i).category + ": " + tags.get(i).value;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tagStrings);
        listTags.setAdapter(adapter);
    }

    public void nextPhoto(View view){
        Photo currentPhoto = currentAlbum.photos.get(currentAlbum.selectedPhoto);
        ArrayList<Tag> tags = currentPhoto.tags;
        if(currentAlbum.photos.size() == 0){}
        else if(currentAlbum.selectedPhoto == currentAlbum.photos.size() - 1){
            currentAlbum.selectedPhoto = 0;
        }
        else{
            currentAlbum.selectedPhoto++;
        }
        setPhoto();
        refreshList();
    }

    public void previousPhoto(View view){
        Photo currentPhoto = currentAlbum.photos.get(currentAlbum.selectedPhoto);
        ArrayList<Tag> tags = currentPhoto.tags;
        if(currentAlbum.photos.size() == 0){}
        else if(currentAlbum.selectedPhoto == 0){
            currentAlbum.selectedPhoto = currentAlbum.photos.size() - 1;
        }
        else{
            currentAlbum.selectedPhoto--;
        }
        setPhoto();
        refreshList();
    }


}