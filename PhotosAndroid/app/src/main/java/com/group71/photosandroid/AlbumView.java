package com.group71.photosandroid;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import com.group71.photosandroid.databinding.ActivityAlbumViewBinding;

public class AlbumView extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    private ActivityAlbumViewBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityAlbumViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        // Unpack albumList parcel
        Intent intent = getIntent();
        AlbumList listOfAlbums = intent.getParcelableExtra("Album List");
    }

    // Create Popup Menu, called in content_album_view.xml
    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.album_view_options);
        popup.show();
    }

    //Actions for Popup Menu Selections
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch(item.getItemId()){
            case R.id.pmAddPhoto:
                Toast.makeText(this, "Option Add Photo selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.pmDeleteAlbum:
                Toast.makeText(this, "Option Delete Photo selected", Toast.LENGTH_SHORT).show();
                return true;
            /*case R.id.pmSlideshow:
                Toast.makeText(this, "Option Slideshow selected", Toast.LENGTH_SHORT).show();
                return true;*/
            default:
                return false;
        }
    }
}