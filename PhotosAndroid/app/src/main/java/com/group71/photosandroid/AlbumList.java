package com.group71.photosandroid;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class AlbumList implements Parcelable, Serializable {
    private static final long serialVersionUID = 1L;
    ArrayList<Album> albumList;
    int albumSelected;

    public AlbumList(ArrayList<Album> listAlbum) {
        albumList = listAlbum;
        albumSelected = -1;
    }

    protected AlbumList(Parcel in) {
        albumList = in.createTypedArrayList(Album.CREATOR);
        albumSelected = in.readInt();
    }

    public ArrayList<Album> getAlbumList(){
        return albumList;
    }

    public int getAlbumSelected(){
        return albumSelected;
    }

    public static final Creator<AlbumList> CREATOR = new Creator<AlbumList>() {
        @Override
        public AlbumList createFromParcel(Parcel in) {
            return new AlbumList(in);
        }

        @Override
        public AlbumList[] newArray(int size) {
            return new AlbumList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(albumList);
        parcel.writeInt(albumSelected);
    }

    public void setAlbumSelected(int albumSelected) {
        this.albumSelected = albumSelected;
    }
}
