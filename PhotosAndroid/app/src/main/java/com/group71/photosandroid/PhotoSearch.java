package com.group71.photosandroid;

public class PhotoSearch {

    public static Album locationSearch(AlbumList albumList, String phrase){
        Album searchResults = new Album("Search Results");
        Photo currentPhoto;

        for(int i = 0; i < albumList.albumList.size(); i++){
            for(int j = 0; j < albumList.albumList.get(i).photos.size(); j++){
                currentPhoto = albumList.albumList.get(i).photos.get(j);
                for(int k = 0; k < currentPhoto.tags.size(); k++){
                    if(currentPhoto.tags.get(k).category.equals("Location") && currentPhoto.tags.get(k).value.toLowerCase().contains(phrase.toLowerCase())){
                        searchResults.photos.add(currentPhoto);
                    }
                }
            }
        }

        return searchResults;
    }

    public static Album personSearch(AlbumList albumList, String phrase){
        Album searchResults = new Album("Search Results");

        Photo currentPhoto;

        for(int i = 0; i < albumList.albumList.size(); i++){
            for(int j = 0; j < albumList.albumList.get(i).photos.size(); j++){
                currentPhoto = albumList.albumList.get(i).photos.get(j);
                for(int k = 0; k < currentPhoto.tags.size(); k++){
                    if(currentPhoto.tags.get(k).category.equals("Person") && currentPhoto.tags.get(k).value.toLowerCase().contains(phrase.toLowerCase())){
                        searchResults.photos.add(currentPhoto);
                    }
                }
            }
        }

        return searchResults;
    }

}
