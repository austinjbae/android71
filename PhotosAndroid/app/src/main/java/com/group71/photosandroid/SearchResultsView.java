package com.group71.photosandroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.group71.photosandroid.databinding.ActivityNewAlbumViewBinding;

import java.io.IOException;
import java.util.ArrayList;

public class SearchResultsView extends AppCompatActivity {
    final Context context = this;

    private AppBarConfiguration appBarConfiguration;
    private ActivityNewAlbumViewBinding binding;

    public static final int Gallery_Request = 1;

    private TableLayout tableLayout;
    Album currentAlbum;
    AlbumList listOfAlbums;

    private final int viewPhotoCode = 2;

    Album searchResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tableLayout = (TableLayout) findViewById(R.id.tableLayout);

        binding = ActivityNewAlbumViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        // Unpack albumList parcel
        Intent intent = getIntent();
        currentAlbum = intent.getParcelableExtra("Current Album");

        refreshList();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if(resultCode != RESULT_OK){
            return;
        }
        System.out.println(requestCode);
        switch(requestCode){
            case Gallery_Request:
                Uri selectedImage = intent.getData();
                listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.add(new Photo(selectedImage));
                refreshList();
                return;
            case viewPhotoCode:
                Bundle photoBundle = intent.getExtras();
                listOfAlbums = (AlbumList) photoBundle.get("Album List");
                refreshList();
                return;
        }


    }

    private void refreshList(){
        tableLayout = new TableLayout(this);
        tableLayout = (TableLayout) findViewById(R.id.tableLayout);
        tableLayout.removeAllViews();
        for (int i = 0; i < currentAlbum.photos.size(); i++) {
            int currentIndex = i;
            ImageView image = new ImageView(this);
            image.setImageURI(currentAlbum.photos.get(i).location);
            image.setAdjustViewBounds(true);
            image.setMaxHeight(200);
            TextView text = new TextView(this);
            text.setText(currentAlbum.photos.get(i).caption);
            text.setTextSize(50);
            text.setPadding(40, 30, 0, 0);
            text.setTextColor(Color.BLACK);

            TableRow row = new TableRow(this);
            row.addView(image);
            row.addView(text);
            row.setPadding(0, 5, 0, 5);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    currentAlbum.selectedPhoto = currentIndex;
                    viewPhoto();
                    //System.out.println("Image Index" + currentIndex);
                }
            });

            tableLayout.addView(row);
        }

    }


    public void viewPhoto(){
        Intent intent = new Intent(SearchResultsView.this, SearchPhotoView.class);
        intent.putExtra("Current Album", (Parcelable) currentAlbum);
        startActivity(intent);
    }



}