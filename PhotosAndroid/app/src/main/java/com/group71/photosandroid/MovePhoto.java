package com.group71.photosandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MovePhoto extends AppCompatActivity {

    AlbumList listOfAlbums;
    ListView albumList;
    int selectedAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_photo);

        Intent intent = getIntent();
        listOfAlbums = intent.getParcelableExtra("Album List");

        refreshList();

        albumList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedAlbum = i;
                moveToAlbum();

            }
        });
    }

    public void refreshList(){
        albumList = new ListView(this);
        albumList = (ListView) findViewById(R.id.listView);

        String[] albumNames = new String[listOfAlbums.albumList.size()];
        for(int i = 0; i < listOfAlbums.albumList.size(); i++){
            albumNames[i] = listOfAlbums.albumList.get(i).name;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, albumNames);
        albumList.setAdapter(adapter);
    }

    public void moveToAlbum(){
        Photo selectedPhoto = listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.get(listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto);
        listOfAlbums.albumList.get(selectedAlbum).photos.add(selectedPhoto);
        listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.remove(listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto);
        Bundle bundle = new Bundle();
        bundle.putParcelable("Album List", listOfAlbums);
        System.out.println(bundle == null);
        Intent currentIntent = new Intent();
        currentIntent.putExtras(bundle);
        setResult(RESULT_OK, currentIntent);
        finish();
    }
}