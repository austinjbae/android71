package com.group71.photosandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class LibraryView extends AppCompatActivity { //implements PopupMenu.OnMenuItemClickListener {
    final Context context = this;

    private ListView listView;
    private Button createAlbum;

    private ArrayList<Album> albumList = new ArrayList<Album>();
    public ArrayList<String> albumNameList = new ArrayList<String>();
    private AlbumList listOfAlbums = new AlbumList(albumList);

    private final int viewAlbumCode = 1;
    ArrayAdapter<String> adapter;

    static final String storeDir = "data";
    static final String storeFile = "albumdata.dat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_view);

        createAlbum = (Button) findViewById(R.id.btnCreate);
        listView = (ListView) findViewById(R.id.listAlbums);

        try {
            listOfAlbums = DataStorage.readData();
        } catch (IOException | ClassNotFoundException e) {
            listOfAlbums = new AlbumList(new ArrayList<Album>());
        }

        // Array Adapter for List View
        /*adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, albumNameList);
        listView = findViewById(R.id.listAlbums);
        listView.setAdapter(adapter);*/

        refreshList();

        // Register Adapter for Context Menu
        registerForContextMenu(listView);

        // ListView Click Listener: Open Album
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                listOfAlbums.setAlbumSelected(i);
                viewAlbum();
            }
        });

        // Button Click Listener: Create Album
        createAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                createAlbum();
                //adapter.notifyDataSetChanged();
            }
        });
    }

    // Create and Show Context Menu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.album_menu, menu);
    }

    // Actions for Context Menu Selections
    //@SuppressLint("NonConstantResourceId")
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.optionRename:
                Toast.makeText(this, "Option rename selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.optionDelete:
                Toast.makeText(this, "Option delete selected", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void createAlbum(){
        LayoutInflater li = LayoutInflater.from(context);
        View newAlbumView = li.inflate(R.layout.create_album,null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(newAlbumView);

        EditText userInput = (EditText) newAlbumView.findViewById(R.id.edittxtAlbumName);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Create Album", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if( !albumNameList.contains(userInput.getText().toString()) &&
                                (userInput.getText().toString() != null) && (userInput.getText().toString().length() != 0))
                        {
                            albumList.add(new Album(userInput.getText().toString()));
                            refreshList();

                        }
                        else
                        {
                            CharSequence text = "Invalid Name";
                            int duration = Toast.LENGTH_SHORT;
                            Toast.makeText(context, text, duration).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id){
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        try {
            DataStorage.writeData(listOfAlbums);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void renameAlbum() {

    }

    public void deleteAlbum() {

    }

    public void viewAlbum(){
        Intent intent = new Intent(LibraryView.this, NewAlbumView.class);
        intent.putExtra("Album List", (Parcelable) listOfAlbums);
        startActivityForResult(intent, viewAlbumCode);

        try {
            DataStorage.writeData(listOfAlbums);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if(resultCode != RESULT_OK){
            return;
        }
        Bundle bundle = intent.getExtras();
        listOfAlbums = (AlbumList) bundle.get("Album List");
        refreshList();
    }

    private void refreshList(){
        albumList = listOfAlbums.albumList;
        String[] albumNames = new String[albumList.size()];
        if(albumList.size() > 0){
            for(int i = 0; i < albumList.size(); i++){
                albumNames[i] = albumList.get(i).name;
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, albumNames);
        listView = findViewById(R.id.listAlbums);
        listView.setAdapter(adapter);
    }
}