package com.group71.photosandroid;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Tag implements Parcelable, Serializable {
    private static final long serialVersionUID = 1L;
    String category;
    String value;

    public Tag(String cat, String val){
        category = cat;
        value = val;
    }

    // Parcelable Constructors + Implementation Methods
    protected Tag(Parcel in) {
        category = in.readString();
        value = in.readString();
    }

    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(category);
        parcel.writeString(value);
    }

    // Setters and Getters
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
