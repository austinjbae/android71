package com.group71.photosandroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.group71.photosandroid.databinding.ActivityNewAlbumViewBinding;

import java.io.IOException;
import java.util.ArrayList;

public class NewAlbumView extends AppCompatActivity {
    final Context context = this;

    private AppBarConfiguration appBarConfiguration;
    private ActivityNewAlbumViewBinding binding;

    public static final int Gallery_Request = 1;

    private TableLayout tableLayout;
    ArrayList<Photo> currentAlbum;
    AlbumList listOfAlbums;

    private final int viewPhotoCode = 2;

    Album searchResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tableLayout = (TableLayout) findViewById(R.id.tableLayout);

        binding = ActivityNewAlbumViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        // Unpack albumList parcel
        Intent intent = getIntent();
        listOfAlbums = intent.getParcelableExtra("Album List");

        ArrayList<Album> albumList = listOfAlbums.getAlbumList();
        Album album = albumList.get(listOfAlbums.getAlbumSelected());
        this.setTitle(album.getName());

        refreshList();
    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.album_view_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.pmAddPhoto:
                //Toast.makeText(this, "Add Photo Selected", Toast.LENGTH_SHORT).show();
                addPhoto();
                return true;
            case R.id.pmDeleteAlbum:
                deleteAlbum();
                finish();
                return true;
            case R.id.pmRenameAlbum:
                renameAlbum();
                try {
                    DataStorage.writeData(listOfAlbums);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //finish();
            case R.id.pmSearch:
                searchAlbums();
                return true;
            default:
                return false;
        }
    }

    //could implement back button in menu bar, not sure if necessary
    @Override
    public boolean onSupportNavigateUp() {
        return true;
    }

    public void deleteAlbum(){
        listOfAlbums.albumList.remove(listOfAlbums.albumSelected);
        try {
            DataStorage.writeData(listOfAlbums);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bundle bundle = new Bundle();
        bundle.putParcelable("Album List", listOfAlbums);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        return;
    }

    public void onBackPressed(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("Album List", listOfAlbums);
        System.out.println(bundle == null);
        Intent currentIntent = new Intent();
        currentIntent.putExtras(bundle);
        setResult(RESULT_OK, currentIntent);
        finish();
    }

    public void renameAlbum(){

        LayoutInflater li = LayoutInflater.from(context);
        View newAlbumView = li.inflate(R.layout.create_album,null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(newAlbumView);

        EditText userInput = (EditText) newAlbumView.findViewById(R.id.edittxtAlbumName);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Rename Album", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if((userInput.getText().toString() != null) && (userInput.getText().toString().length() != 0))
                        {
                            listOfAlbums.albumList.get(listOfAlbums.albumSelected).name = userInput.getText().toString();

                        }
                        else
                        {
                            CharSequence text = "Invalid Name";
                            int duration = Toast.LENGTH_SHORT;
                            Toast.makeText(context, text, duration).show();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id){
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        Bundle bundle = new Bundle();
        bundle.putParcelable("Album List", listOfAlbums);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        return;
    }

    public void addPhoto(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, Gallery_Request);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        if(resultCode != RESULT_OK){
            return;
        }
        System.out.println(requestCode);
        switch(requestCode){
            case Gallery_Request:
                Uri selectedImage = intent.getData();
                listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.add(new Photo(selectedImage));
                try {
                    DataStorage.writeData(listOfAlbums);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                refreshList();
                return;
            case viewPhotoCode:
                Bundle photoBundle = intent.getExtras();
                listOfAlbums = (AlbumList) photoBundle.get("Album List");
                refreshList();
                return;
        }


    }

    private void refreshList(){
            tableLayout = new TableLayout(this);
            tableLayout = (TableLayout) findViewById(R.id.tableLayout);
            tableLayout.removeAllViews();
            for (int i = 0; i < listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.size(); i++) {
                int currentIndex = i;
                ImageView image = new ImageView(this);
                image.setImageURI(listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.get(i).location);
                image.setAdjustViewBounds(true);
                image.setMaxHeight(200);
                TextView text = new TextView(this);
                text.setText(listOfAlbums.albumList.get(listOfAlbums.albumSelected).photos.get(i).caption);
                text.setTextSize(50);
                text.setPadding(40, 30, 0, 0);
                text.setTextColor(Color.BLACK);

                TableRow row = new TableRow(this);
                row.addView(image);
                row.addView(text);
                row.setPadding(0, 5, 0, 5);
                row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listOfAlbums.albumList.get(listOfAlbums.albumSelected).selectedPhoto = currentIndex;
                        viewPhoto();
                        //System.out.println("Image Index" + currentIndex);
                    }
                });

                tableLayout.addView(row);
            }

        try {
            DataStorage.writeData(listOfAlbums);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void viewPhoto(){
        Intent intent = new Intent(NewAlbumView.this, PhotoView.class);
        intent.putExtra("Album List", (Parcelable) listOfAlbums);
        startActivityForResult(intent, viewPhotoCode);
    }

    public void searchAlbums(){


        LayoutInflater li = LayoutInflater.from(context);
        View newAlbumView = li.inflate(R.layout.search_popup,null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(newAlbumView);

        EditText userInput = (EditText) newAlbumView.findViewById(R.id.txtSearchTerm);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("Person", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if( (userInput.getText().toString() != null) && (userInput.getText().toString().length() != 0))
                        {
                            searchResults = PhotoSearch.personSearch(listOfAlbums, userInput.getText().toString());
                            showResults(searchResults);
                        }
                        else
                        {
                            CharSequence text = "Invalid Term";
                            int duration = Toast.LENGTH_SHORT;
                            Toast.makeText(context, text, duration).show();
                        }
                    }
                })
                .setNegativeButton("Location", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id){
                        if( (userInput.getText().toString() != null) && (userInput.getText().toString().length() != 0))
                        {
                            searchResults = PhotoSearch.locationSearch(listOfAlbums, userInput.getText().toString());
                            showResults(searchResults);
                        }
                        else
                        {
                            CharSequence text = "Invalid Name";
                            int duration = Toast.LENGTH_SHORT;
                            Toast.makeText(context, text, duration).show();
                        }
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    public void showResults(Album searchResults){
        Intent intent = new Intent(NewAlbumView.this, SearchResultsView.class);
        intent.putExtra("Current Album", (Parcelable) searchResults);
        startActivity(intent);
    }

}